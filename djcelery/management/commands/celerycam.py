"""

Shortcut to the Django snapshot service.

"""
from optparse import Option

from celery.bin import events
from djcelery.app import app
from djcelery.management.base import CeleryCommand


ev = events.events(app=app)


class Command(CeleryCommand):
    """Run the celery curses event viewer."""
    help = 'Takes snapshots of the clusters state to the database.'
    options = (
        CeleryCommand.options +
        (
            Option('-A', '--app', default=None),
            Option('-b', '--broker', default=None),
            Option('-C', '--no-color', action='store_true', default=None),
            Option('-c', '--camera'),
            Option('-d', '--dump', action='store_true'),
            Option('-F', '--frequency', '--freq', type='float', default=1.0),
            Option('-f', '--logfile', default=None),
            Option('-l', '--loglevel', default='INFO'),
            Option('-q', '--quiet', action='store_true'),
            Option('-r', '--maxrate'),
            Option('--config', default=None),
            Option('--detach', action='store_true'),
            Option('--executable', default=None),
            Option('--gid', default=None),
            Option('--loader', default=None),
            Option('--pidfile', default='celeryev.pid'),
            Option('--uid', default=None),
            Option('--umask', default=None),
            Option('--workdir', default=None, dest='working_directory'),
        ) +
        tuple(ev.app.user_options['events'])
    )

    def handle(self, *args, **options):
        """Handle the management command."""
        options['camera'] = 'djcelery.snapshot.Camera'
        ev.run(*args, **options)
